@extends('layouts.app')

@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.0/angular.js" crossorigin="anonymous"></script>

<div class="container" ng-app="application"  ng-controller="controller" ng-init="cart = <?php echo htmlspecialchars(json_encode($cart)); ?>">
  <table class="table table-striped table-hover">
    <thead>
      <tr>
        <th>Item</th>
        <th>Quantity</th>
        <th>Price</th>
        <th>Total Price</th>
        <th>Operation</th>
      </tr>
    </thead>
    <tbody>
      <tr ng-repeat="row in cart">
        <td class="text-danger">@{{row.name | ucfirst}}</td>
        <td class="text-info"><p ng-hide="@{{row.name}}_row">@{{row.qty}}</p><div dynamic="@{{row.name}}_html"></div></td>
        <td>@{{row.price}}</td>
        <td class="text-success"><p ng-hide="@{{row.name}}_price">@{{row.price * row.qty}}</p><div ng-bind="@{{row.name}}_total"></div></td>
        <td><input type="image" src="images/edit.png" ng-disabled="@{{row.name}}_button" ng-click="toggleQuantity( row )" width="22px" height="22px"> | <a href="cart/delete/@{{row.rowid}}"><img src="images/delete.png" width="22px" height="22px" style="position: absolute;"></a></td>
      </tr>
      <tr>
        <td colspan="4"></td>
        <td><p class="text-primary bg-success"><strong>Grand Total: @{{grandTotal}}<strong></p><div><button class="btn btn-info" data-toggle="modal" data-target="#myModal">
        <img src="https://www.paypal.com/en_US/i/logo/PayPal_mark_37x23.gif" align="left" style="margin-right:7px;"><span class="text" style="font-size:15px; font-family: Arial, Verdana;">Checkout</span>
      </button></div>
        </td>
      </tr>
      <tr>
        <td colspan="4"></td>
        <td></td>
      </tr>
    </tbody>
  </table>
  <div class="modal fade" id="myModal" role="dialog">
    @include('ppform')
  </div>
</div>
<script type="text/javascript" src="js/cart.js"></script>
<script type="text/javascript" src="js/paypal.js"></script>
@endsection
