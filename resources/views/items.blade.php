@extends('layouts.app')

@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.0/angular.js" crossorigin="anonymous"></script>
<div ng-app="myApp" ng-controller="myCtrl">

  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading">Items</div>
          <div class="table-responsive">
            <table class="table">
              <tr>
                @foreach($items as $key=>$res)
                <td>
                  <img src="images/{{$res->image}}" width="100px" height="100px" class="thumb" alt="a picture">
                  <div><a href="" class="btn btn-info btn-sm" role="button" ng-click="addCart(<?php echo htmlspecialchars(json_encode($res)); ?>); showdiv = true" >Add to
                    cart</a></div>
                  </td>
                  @endforeach
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div ng-if="showdiv" class="container">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="panel panel-default">
            <div class="panel-heading">@{{productName | ucfirst}}</div>
            <div class="panel-body">
              <img src="images/@{{productImage}}" width="500px" height="500px" class="thumb" alt="a picture">
              <div class="panel panel-primary" style="float:right">
                <div class="panel-heading">Information</div>
                <div class="panel-body">
                  <table class="table">
                    <tr class="success">
                      <td>
                        <label for="name" class="col-sm-3 control-label">Price</label>
                      </td>
                      <td>
                        <p class="text-info">$@{{productPrice}}</p>
                      </td>
                    </tr>
                    <tr class="danger">
                      <td>
                        <label for="name" class="col-sm-3 control-label">Manufacturer</label>
                      </td>
                      <td>
                        <p class="text-info">@{{productManufacturer | ucfirst}}</p>
                      </td>
                    </tr>
                    <tr class="info">
                      <td>
                        <label for="name" class="col-sm-3 control-label">Quantity</label>
                      </td>
                      <td>
                        <form name="myForm" action="@{{'/item/' + productId}}" method="POST">
                          {!! csrf_field() !!}
                          <input type="text" name="quantity" class="form-control" placeholder="Enter quantity" ng-model="quantity" ng-pattern="/^(0|[1-9][0-9]*)$/" required >
                          <div ng-show="myForm.quantity.$error.pattern">
                            <span>Must be a number</span>
                          </div>
                          <button type="submit" class="btn btn-success" style=" margin-top: 5px;" ng-disabled="myForm.quantity.$invalid">
                            <i class="fa fa-plus"></i> Add to Cart
                          </button>
                        </form>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
  var app = angular.module("myApp", []);

  app.filter('ucfirst', function() {
    return function(val) {
      return name = val.charAt(0).toUpperCase() + val.slice(1);
    };
  });

  app.controller("myCtrl", function($scope) {
    $scope.addCart = function(result) {
      $scope.productName          = result.name;
      $scope.productImage         = result.image;
      $scope.productManufacturer  = result.manufacturer;
      $scope.productPrice         = result.price;
      // $scope.productQuantity      = 1;
      $scope.productId            = result.id;
    }
  });

  </script>

  @endsection
