<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();
Route::get('/home', 'HomeController@index');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('/item', 'ItemsController');
    Route::POST('/item/{id}', 'ItemsController@addToCart');
    Route::get('/cart', 'ItemsController@showCart');
    Route::POST('/cart/{id}', 'ItemsController@updateCart');
    Route::get('/cart/delete/{id}', 'ItemsController@destroy');

    Route::POST('/paypal', 'PaypalController@listPayments');
    Route::get('/done', 'PaypalController@getDone');
    Route::get('/cancel', 'PaypalController@getCancel');
});
