<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use App\Models\Items;
use App\Http\Requests;
use SoapBox\Formatter\Formatter;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($cart = false)
    {
      $items = Items::get();
      return view('items', ['items' => $items, 'cart' => $cart]);
    }

    /**
     * Method the item to the cart.
     *
     * @return boolean
     */
    public function add($item)
    {
      return Cart::add($item);
    }


    /**
     * Add item to the cart.
     *
     * @param  int  $id
     * @param  obj  $req     * @return \Illuminate\Http\Response
     */
    public function addToCart(Request $req, $id)
    {
      $this->validate($req, [
        'quantity' => 'numeric',
      ]);

      $item    = self::getItem($id,$req->quantity);
      self::add($item);

      return back();
    }

    /**
     * Display the specified item in cart.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getItem($id,$quantity)
    {
      $item        = Items::find($id);
      $item['qty'] = $quantity;
      $items       = array('id'=>$item->id, 'name'=>$item->name, 'qty'=>$item->qty, 'price'=>$item->price);

      return $items;
    }

    /**
     * Display the cart items.
     *
     * @param
     * @return
     */
    public function showCart()
    {
      $cart       = Cart::content();
      return view('cart', ['cart' => $cart]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateCart(Request $request, $id)
    {
      $this->validate($request, [
        'qty' => 'required | numeric',
      ]);
      Cart::update($id, $request->qty);
      return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::remove($id);
        return back();
    }

}
