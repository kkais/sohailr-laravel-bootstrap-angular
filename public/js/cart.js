  var app = angular.module("application", []);

  // Filter that Makes the first character capital
  // @param: string
  // @return: string
  app.filter('ucfirst', function() {
    return function(val) {
      return name = val.charAt(0).toUpperCase() + val.slice(1);
    };
  });

  // Directive that inserts HTML dynamically in Element and $compile it
  // @param: HTML
  // @return: Compiled HTML
  app.directive('dynamic', function ($compile) {
    return {
      restrict: 'A',
      replace: true,
      link: function (scope, ele, attrs) {
        scope.$watch(attrs.dynamic, function(html) {
          ele.html(html);
          $compile(ele.contents())(scope);
        });
      }
    };
  });

  // Main Controller
  app.controller("controller", function($scope, $parse, $rootScope) {

    // Function that triggers on EDIT button click
    // @param: Object of cart Item
    // @return:
    $scope.toggleQuantity = function(val){
      var productName = val.name;
      model = $parse(productName+"_row");
      form  = $parse(productName+"_html");
      qty   = $parse(productName+"_newqty");
      model.assign($scope, true); // Hiding the initial price

      form.assign($scope, "<form ng-controller = theForm name=updateForm method=POST action=cart/"+val.rowid+"> <input class=\"form-control\" name=qty type=text ng-change=\"myFunc()\" ng-model="+val.name+"_newqty  ng-pattern=/^(0|[1-9][0-9]*)$/ required><input name=price type=hidden ng-model=price><input name=prod type=hidden ng-model=prod><div ng-show=\"updateForm.qty.$error.pattern\"><span>Must be a number</span></div><button class=\"btn btn-success\" style=\"margin-top: 3px;float:left;\" type=submit ng-disabled=\"updateForm.qty.$invalid\" ><i class=\"fa fa-plus\"></i>Update</button></form>"); // Pass form to Directive
      qty.assign($scope, val.qty); // Passing quantity of item to input field of form

      $scope.price      = val.price; // Passing price of item to hidden input field of form
      $scope.prod       = val.name; // Passing name of item to hidden input field of form

      // Function that disables the other Edit buttons when one edit button is clicked
      // @param:
      // @return:
      $scope.$watch('cart', function () {
        angular.forEach($scope.cart, function(value, key) {
          if(value.name != val.name){
            editButton = $parse(value.name+"_button");
            editButton.assign($scope, true);
          }
        });
      });

    }

    // Function that displays the Grand total on Page Load
    // @param:
    // @return:
    var grand = 0;
    $scope.$watch('cart', function () {
      angular.forEach($scope.cart, function(value, key) {
        grand = grand + value.subtotal;
      },grand);
      $rootScope.grandTotal = grand;
    });

  });

  // Form Controller
  app.controller("theForm", function($rootScope, $scope, $parse ){
    // Function that triggers on changing input field
    // @param:
    // @return:
    $scope.myFunc = function () {
      var prod      = $scope.prod;

      initialPrice  = $parse(prod+"_price");
      initialPrice.assign($rootScope, true); // Hide the initial Price

      dynamicPrice  = $parse(prod+"_total");
      qty           = $parse(prod+"_newqty");
      var parsed    = $parse(qty)($scope); // Get new quantity via input field

      var total     = parsed * $scope.price;

      // Show the update Price
      if(isNaN(total))
        dynamicPrice.assign($rootScope, $scope.price);
      else {
        dynamicPrice.assign($rootScope, total);
      }

      // Show the update Grand total
      total = isNaN(total) ? $scope.price : total;
      $rootScope.grandTotal = total + (function() {
        var subTotal = 0;
        angular.forEach($scope.cart, function(value){
          if(value.name != $scope.prod){
            subTotal =  subTotal + value.subtotal;
          }
        });
        return subTotal;
      })();
    }
  });
